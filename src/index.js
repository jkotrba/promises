import * as worker from './worker';
// Creating and returning promises

/**
 * Return a promise that resolves with the text 'complete'
 */
export function getResult() {
  /** YOUR CODE HERE **/
  let promiseResult;

  return promiseResult;

}

// Return a promise that rejects with an error containing the message 'failed'
export function getResultFails() {
  /** YOUR CODE HERE **/
}

/**
 * Return the result of chaining 2 promises which resolves
 * The first promise should resolve with the sum of the 2 arguments.
 * The second promise should be resolved by doubling the result of the value from the first promise
 *
 * eg. if num1 is 10 and num2 is 5, the result after resolving the second promise should be 40
 */
export function getResultsChained(num1, num2) {
  /** YOUR CODE HERE **/
}

/**
 * Return a promise that is the result of chaining 2 promises where the second one rejects.
 * The first promise should resolve with the sum of the 2 arguments.
 * The second promise should reject with an error object with the message "failed <resultOfFirstPromise>"
 *
 * eg. if num1 is 10, num2 is 5, it should reject with an error that has the message "failed 15"
 */
export function getResultsChainedFails(num1, num2) {
  /** YOUR CODE HERE **/

}


// Promise methods
/**
 * Return the result of running the result1, result2, and result3 promise functions provided by the worker module.
 */
export function getAllResults() {
  /** YOUR CODE HERE **/
}

/**
 * Return the first result of running the result4, result5, and result6 promise functions provided by the worker module.
 */
export function getFirstResult() {
  /** YOUR CODE HERE **/
}

/**
 * Declare and export an async function named getAsyncResult that takes a single argument which is passed to the result7 function
 * whose value is returned after the function is complete
 */
/** YOUR CODE HERE **/

