/**
 * @fileOverview Various promise returning functions that are used in the main index file.
 * Do no modify this module, only the code in index.js
 * @author Jerome Kotrba
 */

import util from 'util';

const delay = util.promisify(setTimeout);

export function result1() {
  return Promise.resolve(5);
}

export function result2() {
  return Promise.resolve(10);
}

export function result3() {
  return Promise.resolve(15);
}

export function result4() {
  return delay(200).then(() => { return 20; });
}

export function result5() {
  return delay(100).then(() => { return 25; });
}

export function result6() {
  return delay(200).then(() => { return 30; });
}

export function result7(txt) {
  return Promise.resolve(txt.toLowerCase());
}
